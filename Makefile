###############################################################################
#
#   #####    ###    #####  ##           ##      ##  S  D
#  ##   ##  ## ##  ##   ## ##          ##       ##  P  E
#  ##      ##   ## ##      ##          ##       ##  R  M
#  ##      #######  #####  ##         ##        ##  I  O
#  ##      ##   ##      ## ##        ##         ##  N
#  ##   ## ##   ## ##   ## ##        ##    ##   ##  G
#   #####  ##   ##  #####  #######  ##      #####
#
#  Clean Architecture Support Library for Java - Spring Demo
#  (c) 2021 Holger Zahnleiter, all rights reserved
#
###############################################################################

.PHONY: clean db-up db-down create-device-packages create-vendors create-products get-all-products get-single-product request-quotation

clean:
	rm -f container.id

db-up: clean
	docker run \
	    -e POSTGRES_DB=product_app -e POSTGRES_USER=dbuser -e POSTGRES_PASSWORD=dbpassword \
	    -d \
	    -p 127.0.0.1:5432:5432/tcp \
	    postgres:10.15-alpine > container.id

CONTAINER_ID=$(shell cat container.id)

db-down:
	docker stop --time 0 ${CONTAINER_ID}
	docker rm ${CONTAINER_ID}

create-device-packages:
	curl -v \
	    -d '{"name":"DIP28", "description":"Dual inline package, 16 pins"}' \
	    -H "Content-Type: application/json" \
	    -X POST "http://localhost:8080/api/v1/device-packages/DIP28"
	curl -v \
	    -d '{"name":"TQFP32", "description":"Thin quad flat package, 32 pins"}' \
	    -H "Content-Type: application/json" \
	    -X POST "http://localhost:8080/api/v1/device-packages/TQFP32"

create-vendors:
	curl -v \
	    -d '{"code":"MICR", "name":"Microchip Technology Inc."}' \
	    -H "Content-Type: application/json" \
	    -X POST "http://localhost:8080/api/v1/vendors/MICR"

create-products:
	curl -v \
	    -d '{"vendorCode":"MICR", "vendorProductNumber":"ATMEGA8A-PU", "name":"ATMega8A-PU", "description":"8-Bit, high performance, low cost microcontroller", "packageName":"DIP28", "pricePerUnit":182}' \
	    -H "Content-Type: application/json" \
	    -X POST "http://localhost:8080/api/v1/products"
	curl -v \
	    -d '{"vendorCode":"MICR", "vendorProductNumber":"ATMEGA32U2-AU", "name":"ATMega32U2-AU", "description":"8-Bit, high performance, low cost microcontroller with USB controller", "packageName":"TQFP32", "pricePerUnit":482}' \
	    -H "Content-Type: application/json" \
	    -X POST "http://localhost:8080/api/v1/products"

get-all-products:
	curl -v -X GET "http://localhost:8080/api/v1/products"

get-single-product:
	curl -v -X GET "http://localhost:8080/api/v1/products/100000000000"

request-quotation:
	curl -v \
	    -d '{"lineItemsToQuote":[ {"productNumber":"100000000000", "quantity":5}, {"productNumber":"100000000001", "quantity":10}]}' \
	    -H "Content-Type: application/json" \
	    -X GET "http://localhost:8080/api/v1/quotes"

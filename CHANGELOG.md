# Changelog

## 1.0.0 - Initial Version (2021-02-08)

### Functional

-  Demonstrates use of casl4j base and casl4j-spring library
-  Demonstractes use of casl4j-archcheck and casl4j-spring's application architecture check Gradle plug-ins

### Non-Functional

-  Gradle build script
-  Publish module and plug-in to GitLab Maven repository
-  Produce coverage report and push to GitLab for display and merge requests
-  Push test report to GitLab for display

# CASL4J - Clean Architecture Support Library for Java - Spring Demo

Demonstrate the use of the Clean Architecture Support Library for Spring.

2021-02-08, Holger Zahnleiter

## Introduction

Not only demonstrates use of projects [casl4j](https://gitlab.com/hzahnlei/casl4j) and [casl4j-spring](https://gitlab.com/hzahnlei/casl4j-spring).
Also uses the accompanying architecture check Gradle tasks:

-  `domainArchitectureCheck` - from [casl4j-archcheck](https://gitlab.com/hzahnlei/casl4j-archcheck)
-  `springApplictationArchitectureCheck` - from [casl4j-spring](https://gitlab.com/hzahnlei/casl4j-spring)

There are some integration tests included herein.
These are using Testcontainers to launch a PostgreSQL container for testing rather than using a in-memory database such as [H2](https://www.h2database.com/html/main.html).
Use of H2 is demonstrated by [casl4j-spring](https://gitlab.com/hzahnlei/casl4j-spring) already.

## Lokal Execution of the Application

You can run the (Spring Boot) application locally for example by executing

```bash
./gradlew bootRun
````

For this to succeed you need to have a PostgreSQL database up and running.
To make things easier a `Makefile` is provided.
You can start a database container like so (assuming you have installed Docker):

```bash
make db-up
```

As a result, a database will be startet.
You may now start teh Spring Boot demo application.
The console is now blocked because it is possessed by the Spring Boot process.
Pro: You can view your Spring Application's log on screen.
Con: You cannot start further programs in that console.
So, just open another console.

In that other console you can run goals from the provided `Makefile`.
For example, you can add or query products (using cURL).

Once you are done, you can just shot down the demo application by pressing CTRL+C to end the Spring Boot process.
To shut down the database just execute

```bash
make db-down
```

## Disclaimer

This is a private, free, open source and non-profit project (see LICENSE file).
Use on your own risk.
I am not liable for any damage caused.

I am a private person, not associated with any companies mentioned herein.

## Credits

The ideas presented herein are not my own.
I am building on the ideas and experience of countless software developers and software architects before me.
Also, I am using many open source and free software and services.
Thank you:

-  [GitLab](https://gitlab.com)
-  [Visual Studio Code](https://code.visualstudio.com) - Microsoft
-  [Eclipse](https://www.eclipse.org/downloads/) - Eclipse Foundation
-  [Git](https://git-scm.com)
-  [Gradle](https://gradle.org)
-  [Maven](https://maven.apache.org) - Apache Software Foundation
-  [ArchUnit](https://www.archunit.org) - TNG Technology Consulting GmbH
-  [Spring Framework](https://spring.io/projects/spring-framework) - VMware
-  [Lombok](https://projectlombok.org)
-  [Testcontainers](https://www.testcontainers.org)
-  [PostgreSQL](https://www.postgresql.org) - The PostgreSQL Development Group
-  [H2 Database](https://www.h2database.com/html/main.html)
-  [Docker](https://www.docker.com) - Docker Inc.
-  [cURL](https://curl.se)
-  Many more that I might have forgotten or even not be aware of using...

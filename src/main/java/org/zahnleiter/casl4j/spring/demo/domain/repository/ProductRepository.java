package org.zahnleiter.casl4j.spring.demo.domain.repository;

import java.util.List;
import java.util.Optional;

import org.zahnleiter.casl4j.domain.repository.DomainRepository;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Product;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductNumber;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainRepository
public interface ProductRepository {

    ProductNumber acquireProductNumber();

    Product create(Product product);

    Optional<Product> byProductNumber(ProductNumber productNumber);

    Product update(Product changedProduct);

    List<Product> allProducts();

}

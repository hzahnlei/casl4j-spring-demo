package org.zahnleiter.casl4j.spring.demo.domain.exception;

import static java.lang.String.format;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.zahnleiter.casl4j.domain.exception.AbstractDomainException;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Vendor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public final class VendorAlreadyListed extends AbstractDomainException {

    private static final long serialVersionUID = 1L;

    public VendorAlreadyListed(final Vendor vendor, final UUID uuid, final OffsetDateTime timeOfOccurrence) {
        super(format("Vendor %s (code %s) already listed.", vendor.name.value, vendor.code.value), uuid,
                timeOfOccurrence);
    }

}

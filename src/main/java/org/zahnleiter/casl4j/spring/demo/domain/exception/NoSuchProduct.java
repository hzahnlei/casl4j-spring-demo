package org.zahnleiter.casl4j.spring.demo.domain.exception;

import static java.lang.String.format;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.zahnleiter.casl4j.domain.exception.AbstractDomainException;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductNumber;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
public final class NoSuchProduct extends AbstractDomainException {

    private static final long serialVersionUID = 1L;

    public NoSuchProduct(final ProductNumber productNumber, final UUID uuid, final OffsetDateTime timeOfOccurrence) {
        super(format("Product with product number %s does not exist.", productNumber.value), uuid, timeOfOccurrence);
    }

}

package org.zahnleiter.casl4j.spring.demo.domain.usecase;

import org.zahnleiter.casl4j.common.OffsetTimeFactory;
import org.zahnleiter.casl4j.common.UuidFactory;
import org.zahnleiter.casl4j.domain.usecase.BusinessTransaction;
import org.zahnleiter.casl4j.domain.usecase.UseCase;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Product;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductNumber;
import org.zahnleiter.casl4j.spring.demo.domain.exception.NoSuchPackagingOption;
import org.zahnleiter.casl4j.spring.demo.domain.exception.NoSuchVendor;
import org.zahnleiter.casl4j.spring.demo.domain.service.CatalogService;
import org.zahnleiter.casl4j.spring.demo.domain.service.DevicePackageService;
import org.zahnleiter.casl4j.spring.demo.domain.service.VendorListService;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@UseCase
@RequiredArgsConstructor
public class UseCaseIncludeNewProductInCatalogImpl implements UseCaseIncludeNewProductInCatalog {

    private final CatalogService catalog;

    private final VendorListService vendorList;

    private final DevicePackageService packagingOption;

    private final UuidFactory uuid;

    private final OffsetTimeFactory timestamp;

    /**
     * Recognize how this simple use-case/business logic spans over different
     * services (may even be separate domains):
     * <ul>
     * <li>Products</li>
     * <li>Device Packages</li>
     * <li>Vendors</li>
     * </ul>
     *
     * In this demo project these services are co-located in the same code base.
     * (Invocation is a native Java function call.) However, they could well be two
     * different REST services. (Invocation may require a network round trip with
     * HTTP/JSON.)
     */
    @Override
    @BusinessTransaction
    public Product include(final Product newProduct) {
        if (this.vendorList.isListed(newProduct.vendorCode)) {
            if (this.packagingOption.exists(newProduct.packageName)) {
                // Acquire and include have side-effects on the database and require to be part
                // of one transaction.
                final var newProductNumber = acquireNewProductNumber();
                return include(newProduct, newProductNumber);
            } else {
                throw new NoSuchPackagingOption(newProduct.packageName, uuid.newUUID(), timestamp.now());
            }
        } else {
            throw new NoSuchVendor(newProduct.vendorCode, uuid.newUUID(), timestamp.now());
        }
    }

    private Product include(final Product newProduct, final ProductNumber productNumber) {
        final var productToBeCreated = Product.builder() //
                .productNumber(productNumber) //
                .vendorCode(newProduct.vendorCode) //
                .vendorProductNumber(newProduct.vendorProductNumber) //
                .name(newProduct.name) //
                .description(newProduct.description) //
                .packageName(newProduct.packageName) //
                .pricePerUnit(newProduct.pricePerUnit) //
                .build();
        return this.catalog.include(productToBeCreated);
    }

    private ProductNumber acquireNewProductNumber() {
        return this.catalog.acquireNewProductNumber();
    }

}

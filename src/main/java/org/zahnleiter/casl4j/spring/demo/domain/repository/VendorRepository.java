package org.zahnleiter.casl4j.spring.demo.domain.repository;

import java.util.Optional;

import org.zahnleiter.casl4j.domain.repository.DomainRepository;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Vendor;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorCode;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainRepository
public interface VendorRepository {

    Vendor add(Vendor newVendor);

    Optional<Vendor> byVendorCode(VendorCode code);

}

package org.zahnleiter.casl4j.spring.demo.domain.entity.type;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.zahnleiter.casl4j.domain.entity.type.DomainType;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainType
@RequiredArgsConstructor
public class ProductNumber {

    public static ProductNumber from(final String value) {
        return new ProductNumber(value);
    }

    @NotBlank(message = "Product number must not be blank.")
    @Size(min = 12, max = 12, message = "Product number must be made up of exactly twelve characters")
    @Pattern(regexp = "1[0-9]+", message = "Product number must contain digits only.")
    public final String value;

}

package org.zahnleiter.casl4j.spring.demo.domain.service;

import java.util.List;
import java.util.Optional;

import org.zahnleiter.casl4j.spring.demo.domain.entity.Product;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductNumber;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
public interface CatalogService {

    ProductNumber acquireNewProductNumber();

    Product include(Product newProduct);

    Optional<Product> byProductNumber(ProductNumber productNumber);

    boolean isAvailable(ProductNumber productNumber);

    boolean isIncluded(ProductNumber productNumber);

    Product update(Product changedProduct);

    List<Product> allProducts();

}

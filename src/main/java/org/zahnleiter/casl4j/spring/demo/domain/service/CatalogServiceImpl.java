package org.zahnleiter.casl4j.spring.demo.domain.service;

import java.util.List;
import java.util.Optional;

import org.zahnleiter.casl4j.common.OffsetTimeFactory;
import org.zahnleiter.casl4j.common.UuidFactory;
import org.zahnleiter.casl4j.domain.service.DomainService;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Product;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductNumber;
import org.zahnleiter.casl4j.spring.demo.domain.exception.ProductNumberAlreadyInUse;
import org.zahnleiter.casl4j.spring.demo.domain.repository.ProductRepository;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainService
@RequiredArgsConstructor
public class CatalogServiceImpl implements CatalogService {

    private final ProductRepository productRepo;

    private final UuidFactory uuid;

    private final OffsetTimeFactory timestamp;

    @Override
    public ProductNumber acquireNewProductNumber() {
        return this.productRepo.acquireProductNumber();
    }

    @Override
    public Product include(final Product newProduct) {
        if (isAvailable(newProduct.productNumber)) {
            return this.productRepo.create(newProduct);
        } else {
            throw new ProductNumberAlreadyInUse(newProduct.productNumber, uuid.newUUID(), timestamp.now());
        }
    }

    @Override
    public Optional<Product> byProductNumber(final ProductNumber productNumber) {
        return this.productRepo.byProductNumber(productNumber);
    }

    @Override
    public boolean isAvailable(final ProductNumber productNumber) {
        return byProductNumber(productNumber).isEmpty();
    }

    @Override
    public boolean isIncluded(final ProductNumber productNumber) {
        return byProductNumber(productNumber).isPresent();
    }

    @Override
    public Product update(final Product changedProduct) {
        return this.productRepo.update(changedProduct);
    }

    @Override
    public List<Product> allProducts() {
        return this.productRepo.allProducts();
    }

}

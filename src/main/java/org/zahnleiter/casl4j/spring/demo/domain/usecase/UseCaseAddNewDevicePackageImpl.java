package org.zahnleiter.casl4j.spring.demo.domain.usecase;

import org.zahnleiter.casl4j.domain.usecase.UseCase;
import org.zahnleiter.casl4j.spring.demo.domain.entity.DevicePackage;
import org.zahnleiter.casl4j.spring.demo.domain.service.DevicePackageService;

import lombok.RequiredArgsConstructor;

/**
 * Normally, I would mark such a class {@code final} to indicate that it is a
 * concete implementation, not meant for derivation. However, Spring does not
 * like some classes to be {@code final} as it needs to sub-class from it.
 * <b>This applies to all the other use case classes herein as well.</b>
 * <p>
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@UseCase
@RequiredArgsConstructor
public class UseCaseAddNewDevicePackageImpl implements UseCaseAddNewDevicePackage {

    private final DevicePackageService packagingOption;

    @Override
    public DevicePackage add(final DevicePackage devicePackage) {
        return this.packagingOption.create(devicePackage);
    }

}

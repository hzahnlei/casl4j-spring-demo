package org.zahnleiter.casl4j.spring.demo.application.persistence.repositoryimpl;

import static org.zahnleiter.casl4j.spring.demo.application.persistence.mapper.DomainToEntityMapper.vendorEntity;
import static org.zahnleiter.casl4j.spring.demo.application.persistence.mapper.EntityToDomainMapper.optionalVendor;
import static org.zahnleiter.casl4j.spring.demo.application.persistence.mapper.EntityToDomainMapper.vendor;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.zahnleiter.casl4j.spring.demo.application.persistence.dao.VendorJpaRepository;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Vendor;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorCode;
import org.zahnleiter.casl4j.spring.demo.domain.repository.VendorRepository;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@Service
@RequiredArgsConstructor
public class VendorRepositoryJpaImpl implements VendorRepository {

    private final VendorJpaRepository repository;

    @Override
    public Vendor add(final Vendor newVendor) {
        return vendor(this.repository.save(vendorEntity(newVendor)));
    }

    @Override
    public Optional<Vendor> byVendorCode(final VendorCode code) {
        return optionalVendor(this.repository.findByCode(code.value));
    }

}

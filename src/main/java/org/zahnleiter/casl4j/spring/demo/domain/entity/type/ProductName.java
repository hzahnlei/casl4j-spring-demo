package org.zahnleiter.casl4j.spring.demo.domain.entity.type;

import javax.validation.constraints.NotBlank;

import org.zahnleiter.casl4j.domain.entity.type.DomainType;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainType
@RequiredArgsConstructor
public class ProductName {

    public static ProductName from(final String value) {
        return new ProductName(value);
    }

    @NotBlank(message = "Product name must not be blank.")
    public final String value;

}

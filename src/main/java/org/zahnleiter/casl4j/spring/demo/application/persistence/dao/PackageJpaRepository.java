package org.zahnleiter.casl4j.spring.demo.application.persistence.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.zahnleiter.casl4j.spring.demo.application.persistence.entity.PackageEntity;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@Repository
public interface PackageJpaRepository
        extends JpaRepository<PackageEntity, Long>, JpaSpecificationExecutor<PackageEntity> {

    Optional<PackageEntity> findByName(final String name);

}

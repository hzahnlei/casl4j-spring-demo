package org.zahnleiter.casl4j.spring.demo.domain.usecase;

import java.util.Optional;

import org.zahnleiter.casl4j.spring.demo.domain.entity.Product;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductNumber;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@FunctionalInterface
public interface UseCaseFindProductByProductNumber {

    Optional<Product> byProductNumber(ProductNumber productNumber);

}

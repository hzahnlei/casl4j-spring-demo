package org.zahnleiter.casl4j.spring.demo.domain.entity.type;

import javax.validation.constraints.NotBlank;

import org.zahnleiter.casl4j.domain.entity.type.DomainType;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@DomainType
@RequiredArgsConstructor
public class VendorName {

    public static VendorName from(final String value) {
        return new VendorName(value);
    }

    @NotBlank(message = "Vendor name must not be blank.")
    public final String value;

}

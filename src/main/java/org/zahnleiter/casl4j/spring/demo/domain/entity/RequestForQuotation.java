package org.zahnleiter.casl4j.spring.demo.domain.entity;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.zahnleiter.casl4j.domain.entity.Aggregate;
import org.zahnleiter.casl4j.domain.entity.DomainEntity;

import lombok.Builder;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainEntity
@Aggregate
@Builder
public class RequestForQuotation {

    @NotNull(message = "List of line items to be quoted is mandatory.")
    @NotEmpty(message = "List of line items to be quoted must not be empty.")
    public final List<@Valid LineItemToQuote> lineItemsToQuote;

}

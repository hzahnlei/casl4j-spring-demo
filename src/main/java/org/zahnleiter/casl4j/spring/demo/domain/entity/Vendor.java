package org.zahnleiter.casl4j.spring.demo.domain.entity;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.zahnleiter.casl4j.domain.entity.Aggregate;
import org.zahnleiter.casl4j.domain.entity.DomainEntity;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorCode;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorName;

import lombok.Builder;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainEntity
@Aggregate
@Builder
public class Vendor {

    @Valid
    @NotNull(message = "Vendor code is mandatory.")
    public final VendorCode code;

    @Valid
    @NotNull(message = "Vendor name is mandatory.")
    public final VendorName name;

}

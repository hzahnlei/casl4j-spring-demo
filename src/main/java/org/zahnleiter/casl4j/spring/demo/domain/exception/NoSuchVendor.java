package org.zahnleiter.casl4j.spring.demo.domain.exception;

import static java.lang.String.format;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.zahnleiter.casl4j.domain.exception.AbstractDomainException;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorCode;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
public final class NoSuchVendor extends AbstractDomainException {

    private static final long serialVersionUID = 1L;

    public NoSuchVendor(final VendorCode vendorCode, final UUID uuid, final OffsetDateTime timeOfOccurrence) {
        super(format("Unknown vendor %s.", vendorCode.value), uuid, timeOfOccurrence);
    }

}

package org.zahnleiter.casl4j.spring.demo.domain.entity.type;

import javax.validation.constraints.PositiveOrZero;

import org.zahnleiter.casl4j.domain.entity.type.DomainType;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainType
@RequiredArgsConstructor
public class AmountInEuroCent {

    public static AmountInEuroCent from(final int value) {
        return new AmountInEuroCent(value);
    }

    @PositiveOrZero(message = "Amount must not be negative.")
    public final int value;

}

package org.zahnleiter.casl4j.spring.demo.domain.entity;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.zahnleiter.casl4j.domain.entity.DomainEntity;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductNumber;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.Quantity;

import lombok.Builder;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainEntity
@Builder
public class LineItemToQuote {

    @Valid
    @NotNull(message = "Product number is mandatory.")
    public final ProductNumber productNumber;

    @Valid
    @NotNull(message = "Quantity must is mandatory.")
    public final Quantity quantity;

}

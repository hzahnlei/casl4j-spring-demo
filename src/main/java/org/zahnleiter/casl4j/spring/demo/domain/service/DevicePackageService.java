package org.zahnleiter.casl4j.spring.demo.domain.service;

import org.zahnleiter.casl4j.spring.demo.domain.entity.DevicePackage;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.PackageName;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
public interface DevicePackageService {

    DevicePackage create(DevicePackage devicePackage);

    boolean exists(PackageName packageName);

}

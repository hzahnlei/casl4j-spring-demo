package org.zahnleiter.casl4j.spring.demo.domain.usecase;

import org.zahnleiter.casl4j.spring.demo.domain.entity.Quotation;
import org.zahnleiter.casl4j.spring.demo.domain.entity.RequestForQuotation;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@FunctionalInterface
public interface UseCaseRequestQuotation {

    Quotation calculateQuotation(RequestForQuotation requestForQuotation);

}

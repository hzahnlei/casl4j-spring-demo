package org.zahnleiter.casl4j.spring.demo.domain.usecase;

import org.zahnleiter.casl4j.spring.demo.domain.entity.DevicePackage;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@FunctionalInterface
public interface UseCaseAddNewDevicePackage {

    DevicePackage add(DevicePackage devicePackage);

}

package org.zahnleiter.casl4j.spring.demo.application.persistence.entity;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@Entity
@Table(name = "product", schema = "product_app")
@Access(AccessType.FIELD)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(name = "product_number")
    public String productNumber;

    @Column(name = "vendor_product_number")
    public String vendorProductNumber;

    @Column(name = "vendor_code")
    public String vendorCode;

    @Column(name = "name")
    public String name;

    @Column(name = "description")
    public String description;

    @Column(name = "package_name")
    public String packageName;

    @Column(name = "price_per_unit")
    public int pricePerUnit;

}

package org.zahnleiter.casl4j.spring.demo.domain.entity;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.zahnleiter.casl4j.domain.entity.DomainEntity;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.AmountInEuroCent;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductNumber;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.Quantity;

import lombok.Builder;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainEntity
@Builder
public class QuotedLineItem {

    @Valid
    @NotNull(message = "Product number is mandatory.")
    public final ProductNumber productNumber;

    @Valid
    @NotNull(message = "Quantity is mandatory.")
    public final Quantity quantity;

    @Valid
    @NotNull(message = "Price per unit is mandatory.")
    public final AmountInEuroCent pricePerUnit;

    @Valid
    @NotNull(message = "Total is mandatory.")
    public final AmountInEuroCent total;

    @Valid
    @NotNull(message = "Discounted total is mandatory.")
    public final AmountInEuroCent discountedTotal;

}

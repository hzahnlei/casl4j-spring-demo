package org.zahnleiter.casl4j.spring.demo.application.api.resource;

import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

/**
 * {@link NoArgsConstructor} is required by the Jackson framework. When
 * deserializing {@link ProductResource}s. As a consequence {@link Builder}
 * requires {@link AllArgsConstructor} and {@link RequiredArgsConstructor} does
 * not work anymore.
 * <br>
 * Furthermore, the member fields can now not be final anymore! However, in
 * opposite to our domain objects, resources aka data transportation objects,
 * are short lived. The are created, sent and gone.
 * <p>
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Builder(access = AccessLevel.PUBLIC)
@NoArgsConstructor
@AllArgsConstructor
public class ProductResource {

    @NotNull(message = "d#############+++++++++++++++++++++++++++")
    public String productNumber;

    public String vendorCode;

    public String vendorProductNumber;

    public String name;

    public String description;

    public String packageName;

    public int pricePerUnit;

}

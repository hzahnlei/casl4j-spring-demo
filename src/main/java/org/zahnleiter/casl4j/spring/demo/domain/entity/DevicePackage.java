package org.zahnleiter.casl4j.spring.demo.domain.entity;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.zahnleiter.casl4j.domain.entity.Aggregate;
import org.zahnleiter.casl4j.domain.entity.DomainEntity;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.PackageDescription;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.PackageName;

import lombok.Builder;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainEntity
@Builder
@Aggregate
public class DevicePackage {

    @Valid
    @NotNull(message = "Package name is mandatory.")
    public final PackageName name;

    @Valid
    @NotNull(message = "Package description is mandatory.")
    public final PackageDescription description;

}

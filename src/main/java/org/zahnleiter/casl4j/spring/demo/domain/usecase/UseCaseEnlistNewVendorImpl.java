package org.zahnleiter.casl4j.spring.demo.domain.usecase;

import org.zahnleiter.casl4j.domain.usecase.UseCase;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Vendor;
import org.zahnleiter.casl4j.spring.demo.domain.service.VendorListService;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@UseCase
@RequiredArgsConstructor
public class UseCaseEnlistNewVendorImpl implements UseCaseEnlistNewVendor {

    private final VendorListService vendorList;

    @Override
    public Vendor enlist(final Vendor newVendor) {
        return this.vendorList.enlist(newVendor);
    }

}

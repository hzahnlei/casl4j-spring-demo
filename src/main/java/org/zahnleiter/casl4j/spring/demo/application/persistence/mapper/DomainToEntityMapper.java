package org.zahnleiter.casl4j.spring.demo.application.persistence.mapper;

import org.zahnleiter.casl4j.spring.demo.application.persistence.entity.PackageEntity;
import org.zahnleiter.casl4j.spring.demo.application.persistence.entity.ProductEntity;
import org.zahnleiter.casl4j.spring.demo.application.persistence.entity.VendorEntity;
import org.zahnleiter.casl4j.spring.demo.domain.entity.DevicePackage;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Product;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Vendor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public final class DomainToEntityMapper {

    private DomainToEntityMapper() {
        // Do not create instances of this.
    }

    public static final ProductEntity productEntity(final Product product) {
        return ProductEntity.builder() //
                .productNumber(product.productNumber.value) //
                .vendorCode(product.vendorCode.value) //
                .vendorProductNumber(product.vendorProductNumber.value) //
                .name(product.name.value) //
                .description(product.description.value) //
                .packageName(product.packageName.value) //
                .pricePerUnit(product.pricePerUnit.value) //
                .build();
    }

    public static final VendorEntity vendorEntity(final Vendor vendor) {
        return VendorEntity.builder() //
                .code(vendor.code.value) //
                .name(vendor.name.value) //
                .build();
    }

    public static final PackageEntity packageEntity(final DevicePackage devicePackage) {
        return PackageEntity.builder() //
                .name(devicePackage.name.value) //
                .description(devicePackage.description.value) //
                .build();
    }

}

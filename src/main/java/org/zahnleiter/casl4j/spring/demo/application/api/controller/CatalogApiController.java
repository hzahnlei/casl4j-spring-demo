package org.zahnleiter.casl4j.spring.demo.application.api.controller;

import static org.zahnleiter.casl4j.spring.demo.application.api.mapper.DomainToResourceMapper.productResource;
import static org.zahnleiter.casl4j.spring.demo.application.api.mapper.DomainToResourceMapper.productResources;
import static org.zahnleiter.casl4j.spring.demo.application.api.mapper.ResourceToDomainMapper.product;

import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.ProductResource;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductNumber;
import org.zahnleiter.casl4j.spring.demo.domain.usecase.UseCaseFindProductByProductNumber;
import org.zahnleiter.casl4j.spring.demo.domain.usecase.UseCaseIncludeNewProductInCatalog;
import org.zahnleiter.casl4j.spring.demo.domain.usecase.UseCaseShowAllProductsFromCatalog;

import lombok.RequiredArgsConstructor;

/**
 * Be advised: In real life we would model our API with openAPI (Swagger). We
 * would then generate resource classes, controllers and clients from that.
 * However, we are building all this here by hand in order not to introduce too
 * many technologies and tools.
 *
 * @author Holger Zahnleiter
 */
@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class CatalogApiController {

    private final UseCaseIncludeNewProductInCatalog useCaseIncludeProduct;

    private final UseCaseFindProductByProductNumber useCaseAccessProduct;

    private final UseCaseShowAllProductsFromCatalog useCaseListProducts;

    /**
     * Prefer the short form:
     *
     * <pre>
     * return productResource(this.useCase.includeInCatalog(product(newProduct)));
     * </pre>
     *
     * For this demo we do it a bit more verbose to explicitly reveal the structure
     * of
     * <ul>
     * <li>mapping between REST resources and domain objects and</li>
     * <li>invoking the domain logic (aka business logic).</li>
     * </ul>
     * <p>
     * Clean Architecture Support Library for Java - Spring Demo
     * <p>
     * (c) 2021 Holger Zahnleiter, all rights reserved
     *
     * @author Holger Zahnleiter
     *
     * @param newProduct to be created
     * @return product actually created
     */
    @PostMapping("/products")
    public ProductResource addNewProduct(@RequestBody final ProductResource newProductResource) {
        // Map incoming REST resource to domain object
        final var newProductDomainEntity = product(newProductResource);
        // Invoke the domain logic
        final var actualProductDomainEntity = this.useCaseIncludeProduct.include(newProductDomainEntity);
        // Map the resulting domain object to a REST resource
        final var actualProductResource = productResource(actualProductDomainEntity);
        // Return result to caller
        return actualProductResource;
    }

    /**
     * For the sake of brevity we do not use paging here. In real life we would
     * stream or use pages. If the data base is really big, then we would have a
     * problem here.
     *
     * @return all products (if any).
     */
    @GetMapping("/products")
    public List<ProductResource> showAllProducts() {
        return productResources(this.useCaseListProducts.allProducts());
    }

    @GetMapping("/products/{productNumber}")
    public HttpEntity<ProductResource> findProductByProductNumber(@PathVariable final String productNumber) {
        final var product = this.useCaseAccessProduct.byProductNumber(ProductNumber.from(productNumber));
        if (product.isPresent()) {
            return ResponseEntity.ok(productResource(product.get()));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}

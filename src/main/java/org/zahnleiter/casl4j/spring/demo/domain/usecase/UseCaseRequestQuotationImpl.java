package org.zahnleiter.casl4j.spring.demo.domain.usecase;

import org.zahnleiter.casl4j.domain.usecase.UseCase;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Quotation;
import org.zahnleiter.casl4j.spring.demo.domain.entity.RequestForQuotation;
import org.zahnleiter.casl4j.spring.demo.domain.service.QuotationService;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@UseCase
@RequiredArgsConstructor
public class UseCaseRequestQuotationImpl implements UseCaseRequestQuotation {

    private final QuotationService quotationCalculator;

    @Override
    public Quotation calculateQuotation(final RequestForQuotation requestForQuotation) {
        return this.quotationCalculator.quotation(requestForQuotation);
    }

}

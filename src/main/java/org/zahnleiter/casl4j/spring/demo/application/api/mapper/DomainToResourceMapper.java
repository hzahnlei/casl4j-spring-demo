package org.zahnleiter.casl4j.spring.demo.application.api.mapper;

import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.zahnleiter.casl4j.spring.demo.application.api.resource.DevicePackageResource;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.ProductResource;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.QuotationResource;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.QuotedLineItemResource;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.VendorResource;
import org.zahnleiter.casl4j.spring.demo.domain.entity.DevicePackage;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Product;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Quotation;
import org.zahnleiter.casl4j.spring.demo.domain.entity.QuotedLineItem;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Vendor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public final class DomainToResourceMapper {

    private DomainToResourceMapper() {
        // Do not create instances of this.
    }

    public static ProductResource productResource(final Product product) {
        return ProductResource.builder() //
                .productNumber(product.productNumber.value) //
                .vendorCode(product.vendorCode.value) //
                .vendorProductNumber(product.vendorProductNumber.value) //
                .name(product.name.value) //
                .description(product.description.value) //
                .packageName(product.packageName.value) //
                .pricePerUnit(product.pricePerUnit.value) //
                .build();
    }

    public static Optional<ProductResource> productResource(final Optional<Product> product) {
        return product.isPresent() //
                ? Optional.of(productResource(product.get())) //
                : Optional.empty();
    }

    public static List<ProductResource> productResources(final Collection<Product> products) {
        return products.stream() //
                .map(DomainToResourceMapper::productResource) //
                .collect(toList());
    }

    public static VendorResource vendorResource(final Vendor vendor) {
        return VendorResource.builder() //
                .code(vendor.code.value) //
                .name(vendor.name.value) //
                .build();
    }

    public static DevicePackageResource devicePackageResource(final DevicePackage devicePackage) {
        return DevicePackageResource.builder() //
                .description(devicePackage.description.value) //
                .name(devicePackage.name.value) //
                .build();
    }

    public static QuotationResource quotationResource(final Quotation quotation) {
        return QuotationResource.builder() //
                .discountedTotalInEuroCents(quotation.discountedTotal().value) //
                .quotedLineItems(quotedLineItemResources(quotation.quotedLineItems)) //
                .totalInEuroCents(quotation.total().value) //
                .build();
    }

    public static List<QuotedLineItemResource> quotedLineItemResources(final List<QuotedLineItem> quotedLineItems) {
        return quotedLineItems.stream() //
                .map(DomainToResourceMapper::quotedLineItem) //
                .collect(toList());
    }

    public static QuotedLineItemResource quotedLineItem(final QuotedLineItem quotedLineItem) {
        return QuotedLineItemResource.builder() //
                .discountedTotalInEuroCent(quotedLineItem.discountedTotal.value) //
                .pricePerUnitInEuroCent(quotedLineItem.pricePerUnit.value) //
                .productNumber(quotedLineItem.productNumber.value) //
                .quantity(quotedLineItem.quantity.value) //
                .totalInEuroCent(quotedLineItem.total.value) //
                .build();
    }

}

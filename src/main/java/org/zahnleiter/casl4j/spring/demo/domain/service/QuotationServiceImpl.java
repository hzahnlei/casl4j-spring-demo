package org.zahnleiter.casl4j.spring.demo.domain.service;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import org.zahnleiter.casl4j.common.OffsetTimeFactory;
import org.zahnleiter.casl4j.common.UuidFactory;
import org.zahnleiter.casl4j.domain.service.DomainService;
import org.zahnleiter.casl4j.spring.demo.domain.entity.LineItemToQuote;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Product;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Quotation;
import org.zahnleiter.casl4j.spring.demo.domain.entity.QuotedLineItem;
import org.zahnleiter.casl4j.spring.demo.domain.entity.RequestForQuotation;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.AmountInEuroCent;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.Quantity;
import org.zahnleiter.casl4j.spring.demo.domain.exception.NoSuchProduct;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainService
@RequiredArgsConstructor
public class QuotationServiceImpl implements QuotationService {

    private final CatalogService catalogService;

    private final UuidFactory uuid;

    private final OffsetTimeFactory timestamp;

    @Override
    public Quotation quotation(final RequestForQuotation requestForQuotation) {
        return Quotation.builder() //
                .quotedLineItems(quotedLineItems(requestForQuotation)) //
                .build();
    }

    private List<QuotedLineItem> quotedLineItems(final RequestForQuotation requestForQuotation) {
        return requestForQuotation.lineItemsToQuote.stream() //
                .map(this::quotationForSingleLineItem) //
                .collect(toList());
    }

    private QuotedLineItem quotationForSingleLineItem(final LineItemToQuote lineItemToQuote) {
        final var product = this.catalogService.byProductNumber(lineItemToQuote.productNumber);
        if (product.isPresent()) {
            return quotedLineItem(lineItemToQuote, product.get());
        } else {
            throw new NoSuchProduct(lineItemToQuote.productNumber, uuid.newUUID(), timestamp.now());
        }
    }

    /**
     * This has been factored out from
     * {@link #quotationForSingleLineItem(LineItemToQuote)}. It could have well
     * stayed there. However, moving it out into a separate function has the
     * following advantages:
     * <ul>
     * <li>{@link #quotationForSingleLineItem(LineItemToQuote)} is now much shorter
     * and more clearly layed out</li>
     * <li>If the body of this function would still be the then-block in
     * {@link #quotationForSingleLineItem(LineItemToQuote)}, then it would be
     * cluttered with {@link Optional#get()}s</li>
     * </ul>
     *
     * @param lineItemToQuote
     * @param product
     * @return
     */
    private QuotedLineItem quotedLineItem(final LineItemToQuote lineItemToQuote, final Product product) {
        return QuotedLineItem.builder() //
                .productNumber(lineItemToQuote.productNumber) //
                .quantity(lineItemToQuote.quantity) //
                .pricePerUnit(product.pricePerUnit) //
                .total(total(lineItemToQuote.quantity, product)) //
                .discountedTotal(discountedTotal(lineItemToQuote.quantity, product)) //
                .build();
    }

    private AmountInEuroCent total(final Quantity quantity, final Product product) {
        return AmountInEuroCent.from(quantity.value * product.pricePerUnit.value);
    }

    /**
     * TODO maybe read the "accretion of discount" table in the use case and pass in
     * as an argument????
     *
     * The accretion of discount would usually be configurable and held in a
     * database table. Furthermore, products are grouped into categories. Each
     * category has its own accretion of discount. For the sake of brevity it is
     * hard coded here.
     *
     * @param quantity of parts to order
     * @param product  price per single unit
     * @return total that takes accretion of discount into account
     */
    private final AmountInEuroCent discountedTotal(final Quantity quantity, final Product product) {
        final var computableQuantity = quantity.value;
        final var computablePricePerUnit = product.pricePerUnit.value;
        if (computableQuantity < 10) {
            return AmountInEuroCent.from((int) (computablePricePerUnit * computableQuantity * 1.0));
        } else if (computableQuantity < 100) {
            return AmountInEuroCent.from((int) (computablePricePerUnit * computableQuantity * 0.98));
        } else {
            return AmountInEuroCent.from((int) (computablePricePerUnit * computableQuantity * 0.95));
        }
    }

}

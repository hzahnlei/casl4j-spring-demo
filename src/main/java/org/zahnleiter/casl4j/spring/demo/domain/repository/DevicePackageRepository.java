package org.zahnleiter.casl4j.spring.demo.domain.repository;

import org.zahnleiter.casl4j.domain.repository.DomainRepository;
import org.zahnleiter.casl4j.spring.demo.domain.entity.DevicePackage;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.PackageName;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainRepository
public interface DevicePackageRepository {

    DevicePackage create(DevicePackage devicePackage);

    boolean exists(PackageName packageName);

}

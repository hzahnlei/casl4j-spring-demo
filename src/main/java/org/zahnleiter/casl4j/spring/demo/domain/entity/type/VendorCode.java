package org.zahnleiter.casl4j.spring.demo.domain.entity.type;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.zahnleiter.casl4j.domain.entity.type.DomainType;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainType
@RequiredArgsConstructor
@EqualsAndHashCode
public class VendorCode {

    public static VendorCode from(final String value) {
        return new VendorCode(value);
    }

    @NotBlank(message = "Vendor code must not be blank.")
    @Size(min = 4, max = 4, message = "Vendor code must be made up of four characters")
    @Pattern(regexp = "[A-Z]+", message = "Vendor code must contain uppercase letters only.")
    public final String value;

}

package org.zahnleiter.casl4j.spring.demo.domain.entity;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.zahnleiter.casl4j.domain.entity.Aggregate;
import org.zahnleiter.casl4j.domain.entity.DomainEntity;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.AmountInEuroCent;

import lombok.Builder;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainEntity
@Aggregate
@Builder
public class Quotation {

    public AmountInEuroCent discountedTotal() {
        return AmountInEuroCent.from(this.quotedLineItems.stream() //
                .mapToInt(lineItem -> lineItem.discountedTotal.value) //
                .sum());
    }

    public AmountInEuroCent total() {
        return AmountInEuroCent.from(this.quotedLineItems.stream() //
                .mapToInt(lineItem -> lineItem.total.value) //
                .sum());
    }

    @NotNull(message = "List of quoted line items is mandatory.")
    @NotEmpty(message = "List of quoted line items must not be empty.")
    public final List<@Valid QuotedLineItem> quotedLineItems;

}

package org.zahnleiter.casl4j.spring.demo.domain.usecase;

import java.util.Optional;

import org.zahnleiter.casl4j.domain.usecase.UseCase;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Product;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductNumber;
import org.zahnleiter.casl4j.spring.demo.domain.service.CatalogService;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@UseCase
@RequiredArgsConstructor
public class UseCaseFindProductByProductNumberImpl implements UseCaseFindProductByProductNumber {

    private final CatalogService catalog;

    @Override
    public Optional<Product> byProductNumber(final ProductNumber productNumber) {
        return this.catalog.byProductNumber(productNumber);
    }

}

package org.zahnleiter.casl4j.spring.demo.application.api.controller;

import static org.zahnleiter.casl4j.spring.demo.application.api.mapper.DomainToResourceMapper.vendorResource;
import static org.zahnleiter.casl4j.spring.demo.application.api.mapper.ResourceToDomainMapper.vendor;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zahnleiter.casl4j.common.OffsetTimeFactory;
import org.zahnleiter.casl4j.common.UuidFactory;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.VendorResource;
import org.zahnleiter.casl4j.spring.demo.application.crosscutting.exception.VendorCodePathMismatch;
import org.zahnleiter.casl4j.spring.demo.domain.usecase.UseCaseEnlistNewVendor;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class VendorListApiController {

    private final UseCaseEnlistNewVendor useCaseEnlistNewVendor;

    private final UuidFactory uuid;

    private final OffsetTimeFactory timestamp;

    @PostMapping("/vendors/{code}")
    public VendorResource enlistNewVendor(@PathVariable final String code,
            @RequestBody final VendorResource newVendor) {
        if (code.equals(newVendor.code)) {
            return vendorResource(useCaseEnlistNewVendor.enlist(vendor(newVendor)));
        } else {
            throw new VendorCodePathMismatch(code, newVendor.code, uuid.newUUID(),
                    timestamp.now());
        }
    }

}

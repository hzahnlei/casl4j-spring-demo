package org.zahnleiter.casl4j.spring.demo.application.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.zahnleiter.casl4j.spring.demo.application.persistence.entity.VendorEntity;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@Repository
public interface VendorJpaRepository extends JpaRepository<VendorEntity, Long>, JpaSpecificationExecutor<VendorEntity> {

    VendorEntity findByCode(final String vendorCode);

}

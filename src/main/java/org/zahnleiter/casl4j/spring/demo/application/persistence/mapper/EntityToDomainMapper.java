package org.zahnleiter.casl4j.spring.demo.application.persistence.mapper;

import java.util.Optional;

import org.zahnleiter.casl4j.spring.demo.application.persistence.entity.PackageEntity;
import org.zahnleiter.casl4j.spring.demo.application.persistence.entity.ProductEntity;
import org.zahnleiter.casl4j.spring.demo.application.persistence.entity.VendorEntity;
import org.zahnleiter.casl4j.spring.demo.domain.entity.DevicePackage;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Product;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Vendor;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.AmountInEuroCent;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.PackageDescription;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.PackageName;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductDescription;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductName;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductNumber;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorCode;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorName;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorProductNumber;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public final class EntityToDomainMapper {

    private EntityToDomainMapper() {
        // Do not create instances of this.
    }

    public static final Product product(final ProductEntity product) {
        return Product.builder() //
                .productNumber(ProductNumber.from(product.productNumber)) //
                .vendorCode(VendorCode.from(product.vendorCode)) //
                .vendorProductNumber(VendorProductNumber.from(product.vendorProductNumber)) //
                .name(ProductName.from(product.name)) //
                .description(ProductDescription.from(product.description)) //
                .packageName(PackageName.from(product.packageName)) //
                .pricePerUnit(AmountInEuroCent.from(product.pricePerUnit)) //
                .build();
    }

    public static final Optional<Product> optionalProduct(final ProductEntity product) {
        return null == product //
                ? Optional.empty()
                : Optional.of(product(product));
    }

    public static final Vendor vendor(final VendorEntity vendor) {
        return Vendor.builder() //
                .code(VendorCode.from(vendor.code)) //
                .name(VendorName.from(vendor.name)) //
                .build();
    }

    public static final Optional<Vendor> optionalVendor(final VendorEntity vendor) {
        return null == vendor //
                ? Optional.empty()
                : Optional.of(vendor(vendor));
    }

    public static final DevicePackage devicePackage(final PackageEntity packageEntity) {
        return DevicePackage.builder() //
                .name(PackageName.from(packageEntity.name)) //
                .description(PackageDescription.from(packageEntity.description)) //
                .build();
    }

}

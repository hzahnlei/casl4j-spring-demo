package org.zahnleiter.casl4j.spring.demo.application.api.controller;

import static org.zahnleiter.casl4j.spring.demo.application.api.mapper.DomainToResourceMapper.quotationResource;
import static org.zahnleiter.casl4j.spring.demo.application.api.mapper.ResourceToDomainMapper.requestForQuotation;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.QuotationResource;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.RequestForQuotationResource;
import org.zahnleiter.casl4j.spring.demo.domain.usecase.UseCaseRequestQuotation;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class QuotationApiController {

    private final UseCaseRequestQuotation useCaseCalculateQuotation;

    @GetMapping("/quotes")
    public QuotationResource quotation(@RequestBody final RequestForQuotationResource requestForQuotation) {
        return quotationResource(
                this.useCaseCalculateQuotation.calculateQuotation(requestForQuotation(requestForQuotation)));
    }

}

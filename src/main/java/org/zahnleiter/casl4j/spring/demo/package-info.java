/**
 * <pre>
 *  #####    ###    #####  ##           ##      ##  S  D
 * ##   ##  ## ##  ##   ## ##          ##       ##  P  E
 * ##      ##   ## ##      ##          ##       ##  R  M
 * ##      #######  #####  ##         ##        ##  I  O
 * ##      ##   ##      ## ##        ##         ##  N
 * ##   ## ##   ## ##   ## ##        ##    ##   ##  G
 *  #####  ##   ##  #####  #######  ##      #####
 *
 * Clean Architecture Support Library for Java - Spring Demo
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * </pre>
 */
package org.zahnleiter.casl4j.spring.demo;

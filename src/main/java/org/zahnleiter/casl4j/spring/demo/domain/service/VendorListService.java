package org.zahnleiter.casl4j.spring.demo.domain.service;

import java.util.Optional;

import org.zahnleiter.casl4j.spring.demo.domain.entity.Vendor;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorCode;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
public interface VendorListService {

    Vendor enlist(Vendor newVendor);

    Optional<Vendor> byVendorCode(VendorCode code);

    boolean isListed(VendorCode code);

}

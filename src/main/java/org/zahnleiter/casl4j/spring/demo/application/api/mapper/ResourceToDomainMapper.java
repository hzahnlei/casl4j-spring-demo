package org.zahnleiter.casl4j.spring.demo.application.api.mapper;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.zahnleiter.casl4j.spring.demo.application.api.resource.DevicePackageResource;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.LineItemToQuoteResource;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.ProductResource;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.RequestForQuotationResource;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.VendorResource;
import org.zahnleiter.casl4j.spring.demo.domain.entity.DevicePackage;
import org.zahnleiter.casl4j.spring.demo.domain.entity.LineItemToQuote;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Product;
import org.zahnleiter.casl4j.spring.demo.domain.entity.RequestForQuotation;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Vendor;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.AmountInEuroCent;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.PackageDescription;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.PackageName;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductDescription;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductName;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductNumber;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.Quantity;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorCode;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorName;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorProductNumber;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public final class ResourceToDomainMapper {

    private ResourceToDomainMapper() {
        // Do not create instances of this.
    }

    public static Product product(final ProductResource product) {
        return Product.builder() //
                .productNumber(ProductNumber.from(product.productNumber)) //
                .vendorCode(VendorCode.from(product.vendorCode)) //
                .vendorProductNumber(VendorProductNumber.from(product.vendorProductNumber)) //
                .name(ProductName.from(product.name)) //
                .description(ProductDescription.from(product.description)) //
                .packageName(PackageName.from(product.packageName)) //
                .pricePerUnit(AmountInEuroCent.from(product.pricePerUnit)) //
                .build();
    }

    public static Vendor vendor(final VendorResource vendor) {
        return Vendor.builder() //
                .code(VendorCode.from(vendor.code.toUpperCase())) //
                .name(VendorName.from(vendor.name)) //
                .build();
    }

    public static DevicePackage devicePackage(final DevicePackageResource devicePackage) {
        return DevicePackage.builder() //
                .description(PackageDescription.from(devicePackage.description)) //
                .name(PackageName.from(devicePackage.name)) //
                .build();
    }

    public static RequestForQuotation requestForQuotation(final RequestForQuotationResource requestForQuotation) {
        return RequestForQuotation.builder() //
                .lineItemsToQuote(lineItemsToQuote(requestForQuotation.lineItemsToQuote)) //
                .build();
    }

    public static List<LineItemToQuote> lineItemsToQuote(final List<LineItemToQuoteResource> lineItemsToQuote) {
        return lineItemsToQuote.stream() //
                .map(ResourceToDomainMapper::lineItemToQuote) //
                .collect(toList());
    }

    public static LineItemToQuote lineItemToQuote(final LineItemToQuoteResource lineItemToQuote) {
        return LineItemToQuote.builder() //
                .productNumber(ProductNumber.from(lineItemToQuote.productNumber)) //
                .quantity(Quantity.from(lineItemToQuote.quantity)) //
                .build();
    }

}

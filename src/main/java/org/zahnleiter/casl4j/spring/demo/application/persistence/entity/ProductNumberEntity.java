package org.zahnleiter.casl4j.spring.demo.application.persistence.entity;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.zahnleiter.casl4j.spring.demo.application.persistence.dao.ProductNumberJpaRepository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

/**
 * The {@code @NoArgsConstructor} and {@code @AllArgsConstructor} annotations
 * were required the moment we added the
 * {@link ProductNumberJpaRepository#deleteAll()} to our integrative test.
 * <br>
 * <b>Tests must have no influence on the code under test.</b> In this case we
 * accept the impact as it is small. To get rid of this we would have to start a
 * fresh database container for every single test case. That would cause long
 * testing times. Instead, we clean all tables and reset some sequences before
 * each test case. Which is much faster.
 * <p>
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Entity
@Table(name = "product_number", schema = "product_app")
@Access(AccessType.FIELD)
@Builder
@NoArgsConstructor // Required by Hibernate
@AllArgsConstructor // Required by @Builder as soon as we use @NoArgsConstructor
public class ProductNumberEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

}

package org.zahnleiter.casl4j.spring.demo.application.persistence.repositoryimpl;

import static java.util.stream.Collectors.toList;
import static org.zahnleiter.casl4j.spring.demo.application.persistence.mapper.DomainToEntityMapper.productEntity;
import static org.zahnleiter.casl4j.spring.demo.application.persistence.mapper.EntityToDomainMapper.optionalProduct;
import static org.zahnleiter.casl4j.spring.demo.application.persistence.mapper.EntityToDomainMapper.product;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.zahnleiter.casl4j.spring.demo.application.persistence.dao.ProductJpaRepository;
import org.zahnleiter.casl4j.spring.demo.application.persistence.dao.ProductNumberJpaRepository;
import org.zahnleiter.casl4j.spring.demo.application.persistence.entity.ProductNumberEntity;
import org.zahnleiter.casl4j.spring.demo.application.persistence.mapper.EntityToDomainMapper;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Product;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductNumber;
import org.zahnleiter.casl4j.spring.demo.domain.repository.ProductRepository;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@Service
@RequiredArgsConstructor
public class ProductRepositoryJpaImpl implements ProductRepository {

    private final ProductJpaRepository productRepo;

    private final ProductNumberJpaRepository productNumbersRepo;

    @Override
    public Product create(final Product product) {
        return product(this.productRepo.save(productEntity(product)));
    }

    @Override
    public ProductNumber acquireProductNumber() {
        final var productNumber = ProductNumberEntity.builder().build();
        this.productNumbersRepo.save(productNumber);
        return new ProductNumber(Long.toString(productNumber.id));
    }

    @Override
    public Optional<Product> byProductNumber(final ProductNumber productNumber) {
        return optionalProduct(this.productRepo.findByProductNumber(productNumber.value));
    }

    @Override
    public Product update(final Product changedProduct) {
        final var currentProductEntity = this.productRepo.findByProductNumber(changedProduct.productNumber.value);
        currentProductEntity.vendorCode = changedProduct.vendorCode.value;
        currentProductEntity.vendorProductNumber = changedProduct.vendorProductNumber.value;
        // No explicit save required since these are Spring managed entities
        return product(currentProductEntity);
    }

    @Override
    public List<Product> allProducts() {
        return this.productRepo.findAll().stream() //
                .map(EntityToDomainMapper::product) //
                .collect(toList());
    }

}

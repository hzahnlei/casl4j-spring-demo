package org.zahnleiter.casl4j.spring.demo.domain.service;

import java.util.Optional;

import org.zahnleiter.casl4j.common.OffsetTimeFactory;
import org.zahnleiter.casl4j.common.UuidFactory;
import org.zahnleiter.casl4j.domain.service.DomainService;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Product;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Vendor;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorCode;
import org.zahnleiter.casl4j.spring.demo.domain.exception.VendorAlreadyListed;
import org.zahnleiter.casl4j.spring.demo.domain.repository.VendorRepository;
import org.zahnleiter.casl4j.spring.demo.domain.usecase.UseCaseIncludeNewProductInCatalogImpl;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainService
@RequiredArgsConstructor
public class VendorListServiceImpl implements VendorListService {

    private final VendorRepository vendorRepo;

    private final UuidFactory uuid;

    private final OffsetTimeFactory timestamp;

    /**
     * In opposite to {@link UseCaseIncludeNewProductInCatalogImpl#include(Product)}
     * the super-simple business logic contained herein does not span multiple
     * services (domains). It focuses only on its own domain and the objects of that
     * domain.
     */
    @Override
    public Vendor enlist(final Vendor newVendor) {
        if (!isListed(newVendor.code)) {
            return this.vendorRepo.add(newVendor);
        } else {
            throw new VendorAlreadyListed(newVendor, uuid.newUUID(), timestamp.now());
        }
    }

    @Override
    public Optional<Vendor> byVendorCode(final VendorCode code) {
        return this.vendorRepo.byVendorCode(code);
    }

    /**
     * Not all business logic directly relies on the underlying repository. Domain
     * services are not just boiler-plate for repositories.
     *
     * In the real world (outside our simple demo) business logic can grow quite
     * complex.
     */
    @Override
    public boolean isListed(final VendorCode code) {
        return byVendorCode(code).isPresent();
    }

}

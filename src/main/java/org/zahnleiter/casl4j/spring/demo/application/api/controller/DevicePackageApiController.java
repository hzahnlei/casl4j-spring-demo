package org.zahnleiter.casl4j.spring.demo.application.api.controller;

import static org.zahnleiter.casl4j.spring.demo.application.api.mapper.DomainToResourceMapper.devicePackageResource;
import static org.zahnleiter.casl4j.spring.demo.application.api.mapper.ResourceToDomainMapper.devicePackage;

import java.time.OffsetDateTime;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zahnleiter.casl4j.common.TimeFactory;
import org.zahnleiter.casl4j.common.UuidFactory;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.DevicePackageResource;
import org.zahnleiter.casl4j.spring.demo.application.crosscutting.exception.DevicePackageNamePathMismatch;
import org.zahnleiter.casl4j.spring.demo.domain.usecase.UseCaseAddNewDevicePackage;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class DevicePackageApiController {

    private final UseCaseAddNewDevicePackage useCaseAddNewPackage;

    private final UuidFactory uuid;

    private final TimeFactory<OffsetDateTime> timestamp;

    @PostMapping("/device-packages/{name}")
    public DevicePackageResource addNewDevicePackage(@PathVariable final String name,
            @RequestBody final DevicePackageResource newDevicePackage) {
        if (name.equals(newDevicePackage.name)) {
            return devicePackageResource(useCaseAddNewPackage.add(devicePackage(newDevicePackage)));
        } else {
            throw new DevicePackageNamePathMismatch(name, newDevicePackage.name, uuid.newUUID(),
                    timestamp.now());
        }
    }

}

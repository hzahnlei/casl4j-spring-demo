package org.zahnleiter.casl4j.spring.demo.application.crosscutting.exception;

import static java.lang.String.format;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.zahnleiter.casl4j.application.exception.AbstractTechnicalException;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public final class VendorCodePathMismatch extends AbstractTechnicalException {

    private static final long serialVersionUID = 1L;

    public VendorCodePathMismatch(final String codeFromPath, final String codeFromResource, final UUID id,
            final OffsetDateTime timeOfOccurrence) {
        super(format("Path of vendor resource (%s) and vendor code (%s) have to be identical.", codeFromPath,
                codeFromResource), id, timeOfOccurrence);
    }

}

package org.zahnleiter.casl4j.spring.demo.domain.usecase;

import org.zahnleiter.casl4j.spring.demo.domain.entity.Product;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
// Could be @BeanValidated
@FunctionalInterface
public interface UseCaseIncludeNewProductInCatalog {

    // Could be @Valid
    // Or, could be Product include(@Valid Product newProduct);
    Product include(Product newProduct);

}

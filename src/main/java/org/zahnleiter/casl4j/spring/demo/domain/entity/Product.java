package org.zahnleiter.casl4j.spring.demo.domain.entity;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.zahnleiter.casl4j.domain.entity.Aggregate;
import org.zahnleiter.casl4j.domain.entity.DomainEntity;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.AmountInEuroCent;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.PackageName;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductDescription;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductName;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductNumber;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorCode;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorProductNumber;

import lombok.Builder;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainEntity
@Aggregate
@Builder
public class Product {

    @Valid
    @NotNull(message = "Product number is mandatory.")
    public final ProductNumber productNumber;

    @Valid
    @NotNull(message = "Vendor product code is mandatory.")
    public final VendorProductNumber vendorProductNumber;

    @Valid
    @NotNull(message = "Vendor code is mandatory.")
    public final VendorCode vendorCode;

    @Valid
    @NotNull(message = "Product name is mandatory.")
    public final ProductName name;

    @Valid
    @NotNull(message = "Product description is mandatory.")
    public final ProductDescription description;

    @Valid
    @NotNull(message = "Package name is mandatory.")
    public final PackageName packageName;

    @Valid
    @NotNull(message = "Price per unit is mandatory.")
    public final AmountInEuroCent pricePerUnit;

}

package org.zahnleiter.casl4j.spring.demo.application.persistence.repositoryimpl;

import static org.zahnleiter.casl4j.spring.demo.application.persistence.mapper.DomainToEntityMapper.packageEntity;
import static org.zahnleiter.casl4j.spring.demo.application.persistence.mapper.EntityToDomainMapper.devicePackage;

import org.springframework.stereotype.Service;
import org.zahnleiter.casl4j.spring.demo.application.persistence.dao.PackageJpaRepository;
import org.zahnleiter.casl4j.spring.demo.domain.entity.DevicePackage;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.PackageName;
import org.zahnleiter.casl4j.spring.demo.domain.repository.DevicePackageRepository;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@Service
@RequiredArgsConstructor
public class PackageRepositoryJpaImpl implements DevicePackageRepository {

    private final PackageJpaRepository packageRepo;

    @Override
    public DevicePackage create(final DevicePackage devicePackage) {
        return devicePackage(this.packageRepo.save(packageEntity(devicePackage)));
    }

    @Override
    public boolean exists(final PackageName packageName) {
        return this.packageRepo.findByName(packageName.value).isPresent();
    }

}

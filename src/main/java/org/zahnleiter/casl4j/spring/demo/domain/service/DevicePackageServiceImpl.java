package org.zahnleiter.casl4j.spring.demo.domain.service;

import org.zahnleiter.casl4j.domain.service.DomainService;
import org.zahnleiter.casl4j.spring.demo.domain.entity.DevicePackage;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.PackageName;
import org.zahnleiter.casl4j.spring.demo.domain.repository.DevicePackageRepository;

import lombok.RequiredArgsConstructor;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 * 
 * @author Holger Zahnleiter
 */
@DomainService
@RequiredArgsConstructor
public class DevicePackageServiceImpl implements DevicePackageService {

    private final DevicePackageRepository devicePackageRepo;

    @Override
    public DevicePackage create(final DevicePackage devicePackage) {
        return this.devicePackageRepo.create(devicePackage);
    }

    @Override
    public boolean exists(final PackageName packageName) {
        return this.devicePackageRepo.exists(packageName);
    }

}

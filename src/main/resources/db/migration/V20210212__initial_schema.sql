--   #####    ###    #####  ##           ##      ##  S  D
--  ##   ##  ## ##  ##   ## ##          ##       ##  P  E
--  ##      ##   ## ##      ##          ##       ##  R  M
--  ##      #######  #####  ##         ##        ##  I  O
--  ##      ##   ##      ## ##        ##         ##  N
--  ##   ## ##   ## ##   ## ##        ##    ##   ##  G
--   #####  ##   ##  #####  #######  ##      #####
--
--  Clean Architecture Support Library for Java - Spring Demo
--  (c) 2021 Holger Zahnleiter, all rights reserved

CREATE SCHEMA IF NOT EXISTS product_app;


CREATE TABLE IF NOT EXISTS product_app.vendor (
	id   BIGSERIAL PRIMARY KEY,
	code VARCHAR(4) UNIQUE NOT NULL,
	name VARCHAR UNIQUE NOT NULL
);


CREATE SEQUENCE IF NOT EXISTS product_app.product_number_id_seq
	AS BIGINT
	START WITH 100000000000
	MAXVALUE   999999999999;

CREATE TABLE IF NOT EXISTS product_app.product_number (
	id BIGINT NOT NULL DEFAULT nextval('product_app.product_number_id_seq') PRIMARY KEY
);

ALTER SEQUENCE product_app.product_number_id_seq OWNED BY product_app.product_number.id;


CREATE TABLE IF NOT EXISTS product_app.package (
	id                    BIGSERIAL PRIMARY KEY,
	name                  VARCHAR(20) UNIQUE NOT NULL,
	description           VARCHAR(2000) NOT NULL
);


CREATE TABLE IF NOT EXISTS product_app.product (
	id                    BIGSERIAL PRIMARY KEY,
	product_number        VARCHAR(12) UNIQUE NOT NULL,
	vendor_product_number VARCHAR(20) NOT NULL,
	vendor_code           VARCHAR(4) NOT NULL,
		UNIQUE (vendor_product_number, vendor_code),
	name                  VARCHAR(40) NOT NULL,
	description           VARCHAR(2000) NOT NULL,
	package_name          VARCHAR(20) NOT NULL,
	price_per_unit        INTEGER NOT NULL
);

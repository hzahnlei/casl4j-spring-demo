package org.zahnleiter.casl4j.spring.demo.testtools;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

import org.zahnleiter.casl4j.spring.demo.domain.entity.DevicePackage;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Product;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Vendor;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.AmountInEuroCent;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.PackageDescription;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.PackageName;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductDescription;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductName;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.ProductNumber;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorCode;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorName;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorProductNumber;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public final class DomainTestData {

    public static final UUID UUID_1 = UUID.fromString("10000000-1000-1000-1000-100000000000");

    public static final OffsetDateTime TIMESTAMP_1 = OffsetDateTime.of(2020, 03, 25, 15, 02, 0, 0, ZoneOffset.UTC);

    public static Product new_product(final String vendorCode, final String vendorProductNumber, final String name,
            final String description, final String packageName, final int pricePerUnit) {
        return Product.builder() //
                .vendorCode(VendorCode.from(vendorCode)) //
                .vendorProductNumber(VendorProductNumber.from(vendorProductNumber)) //
                .name(ProductName.from(name)) //
                .description(ProductDescription.from(description)) //
                .packageName(PackageName.from(packageName)) //
                .pricePerUnit(AmountInEuroCent.from(pricePerUnit)) //
                .build();
    }

    public static Product product(final String vendorCode, final String vendorProductNumber, final String name,
            final String description, final String packageName, final int pricePerUnit) {
        return new_product(vendorCode, vendorProductNumber, name, description, packageName, pricePerUnit);
    }

    public static Product product(final String ourProductNumber, final String vendorCode,
            final String vendorProductNumber, final String name, final String description, final String packageName,
            final int pricePerUnit) {
        return Product.builder() //
                .productNumber(ProductNumber.from(ourProductNumber)) //
                .vendorCode(VendorCode.from(vendorCode)) //
                .vendorProductNumber(VendorProductNumber.from(vendorProductNumber)) //
                .name(ProductName.from(name)) //
                .description(ProductDescription.from(description)) //
                .packageName(PackageName.from(packageName)) //
                .pricePerUnit(AmountInEuroCent.from(pricePerUnit)) //
                .build();
    }

    public static Vendor vendor(final String code, final String name) {
        return Vendor.builder() //
                .code(VendorCode.from(code)) //
                .name(VendorName.from(name)) //
                .build();
    }

    public static DevicePackage devicePackage(final String name) {
        return DevicePackage.builder() //
                .name(PackageName.from(name)) //
                .description(PackageDescription.from("FIXME")) //
                .build();
    }

    public static DevicePackage devicePackage(final String name, final String description) {
        return DevicePackage.builder() //
                .name(PackageName.from(name)) //
                .description(PackageDescription.from(description)) //
                .build();
    }

}

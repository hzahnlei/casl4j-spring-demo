package org.zahnleiter.casl4j.spring.demo;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;
import static org.zahnleiter.casl4j.spring.demo.testtools.ResourceTestData.devicePackage;
import static org.zahnleiter.casl4j.spring.demo.testtools.ResourceTestData.new_product;
import static org.zahnleiter.casl4j.spring.demo.testtools.ResourceTestData.product;
import static org.zahnleiter.casl4j.spring.demo.testtools.ResourceTestData.vendor;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
// Will be org.springframework.boot.test.web.server.LocalServerPort in Spring 3.0
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.DevicePackageResource;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.ProductResource;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.VendorResource;
import org.zahnleiter.casl4j.spring.demo.application.persistence.dao.PackageJpaRepository;
import org.zahnleiter.casl4j.spring.demo.application.persistence.dao.ProductJpaRepository;
import org.zahnleiter.casl4j.spring.demo.application.persistence.dao.ProductNumberJpaRepository;
import org.zahnleiter.casl4j.spring.demo.application.persistence.dao.VendorJpaRepository;

/**
 * Notice all the annotations and the complexity. Many prerequisites have to be
 * met in order to have these tests succeed. The application hat to be
 * implemented almost complete.
 * <p>
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Tag("IT")
@Tag("slow")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@SpringBootTest(classes = { Application.class }, webEnvironment = DEFINED_PORT)
@Testcontainers
@TestPropertySource(properties = { "logging.level.org.springframework.web.client.RestTemplate=DEBUG",
        "logging.level.org.apache.http=DEBUG", "logging.level.httpclient.wire=DEBUG" })
@Sql(statements = { "ALTER SEQUENCE product_app.product_number_id_seq RESTART WITH 100000000000;" })
class ApplicationEnd2EndIT {

    // ---- TESTCASES ---------------------------------------------------------

    @Test
    void Post_product() {
        Given(vendor("MICR", "Microchip Technology Inc."));
        Given(devicePackage("DIP28", "Dual in-line package, 28 pins"));

        When_we_issue_an_HTTP_POST_request( //
                "api/v1/products", //
                new_product("MICR", "ATMEGA8A-PU", "ATMega8A-PU", "8-Bit, high performance, low cost microcontroller",
                        "DIP28", 182));

        Then_this_succeeds(HttpStatus.OK);
    }

    @Test
    void Get_product_by_product_number() {
        Given(vendor("MICR", "Microchip Technology Inc."));
        Given(devicePackage("DIP28", "Dual in-line package, 28 pins"));
        Given(product("MICR", "ATMEGA8A-PU", "ATMega8A-PU", "8-Bit, high performance, low cost microcontroller",
                "DIP28", 182));

        When_we_issue_an_HTTP_GET_request("api/v1/products/100000000000");

        Then_this_succeeds(HttpStatus.OK);
        Then_we_get_back_this(product("100000000000", "MICR", "ATMEGA8A-PU", "ATMega8A-PU",
                "8-Bit, high performance, low cost microcontroller", "DIP28", 182));
    }

    @Test
    void Get_product_by_unknown_product_number() {
        Given(vendor("MICR", "Microchip Technology Inc."));
        Given(devicePackage("DIP28", "Dual in-line package, 28 pins"));
        Given(product("MICR", "ATMEGA8A-PU", "ATMega8A-PU", "8-Bit, high performance, low cost microcontroller",
                "DIP28", 182));

        When_we_issue_an_HTTP_GET_request("api/v1/products/007");

        Then_this_fails_because(HttpStatus.NOT_FOUND);
    }

    @Test
    void Get_all_products() {
        Given(vendor("MICR", "Microchip Technology Inc."));
        Given(devicePackage("DIP28", "Dual in-line package, 28 pins"));
        Given(devicePackage("TQFP32", "Thin quad flat pack, 32 pins"));
        Given(product("MICR", "ATMEGA8A-PU", "ATMega8A-PU", "8-Bit, high performance, low cost microcontroller",
                "DIP28", 182));
        Given(product("MICR", "ATMEGA32U2-AU", "ATMega32U2-AU",
                "8-Bit, high performance, low cost microcontroller with USB controller", "TQFP32", 482));

        When_we_issue_an_HTTP_GET_request_for_multiple_products("api/v1/products");

        Then_this_succeeds(HttpStatus.OK);
        Then_we_get_back_these( //
                product("100000000000", "MICR", "ATMEGA8A-PU", "ATMega8A-PU",
                        "8-Bit, high performance, low cost microcontroller", "DIP28", 182), //
                product("100000000001", "MICR", "ATMEGA32U2-AU", "ATMega32U2-AU",
                        "8-Bit, high performance, low cost microcontroller with USB controller", "TQFP32", 482));
    }

    // ---- IMPLEMENTATION DETAILS --------------------------------------------

    private static PostgreSQLContainer<?> database;

    @SuppressWarnings("resource")
    @BeforeAll
    private static void setUp() {
        database = new PostgreSQLContainer<>(DockerImageName.parse("postgres:10.15-alpine")) //
                .withUsername("dbuser") //
                .withPassword("dbpassword") //
                .withDatabaseName("product_app") //
                .withExposedPorts(5432);
        database.start();
        System.setProperty("spring.datasource.url", format(database.getJdbcUrl()));
    }

    @AfterAll
    private static void tearDown() {
        database.stop();
        database.close();
    }

    @Autowired
    private VendorJpaRepository vendorJpaRepo;

    @Autowired
    private ProductJpaRepository productJpaRepo;

    @Autowired
    private ProductNumberJpaRepository productNumberJpaRepo;

    @Autowired
    private PackageJpaRepository packageJpaRepo;

    @BeforeEach
    private void wipeTables() {
        this.vendorJpaRepo.deleteAll();
        this.productJpaRepo.deleteAll();
        this.productNumberJpaRepo.deleteAll();
        this.packageJpaRepo.deleteAll();
    }

    @LocalServerPort
    private int port;

    private String apiBaseUrl() {
        return format("http://localhost:%d/api/v1", this.port);
    }

    private void Given(final VendorResource vendor) {
        final var restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        final var response = restTemplate.postForEntity(postVendorUrl(vendor.code), vendor, VendorResource.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    private String postVendorUrl(final String vendorCode) {
        return format("%s/vendors/%s", apiBaseUrl(), vendorCode);
    }

    private Exception unexpectedCause;

    private HttpStatus actualHttpStatus;

    private ProductResource actualProductResource;

    private void When_we_issue_an_HTTP_POST_request(final String productUrl, final ProductResource product) {
        try {
            this.unexpectedCause = null;
            this.actualHttpStatus = null;
            this.actualProductResource = null;
            final var restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
            final var response = restTemplate.postForEntity(effectiveUrl(productUrl), product, ProductResource.class);
            this.actualHttpStatus = response.getStatusCode();
            this.actualProductResource = response.getBody();
        } catch (final HttpServerErrorException cause) {
            this.actualHttpStatus = cause.getStatusCode();
        } catch (final Exception cause) {
            this.unexpectedCause = cause;
        }
    }

    private String effectiveUrl(final String apiUrl) {
        return format("http://localhost:%d/%s", this.port, apiUrl);
    }

    private void Then_this_succeeds(final HttpStatus expectedHttpStatus) {
        assertNull(this.unexpectedCause);
        assertNotNull(this.actualHttpStatus);
        assertEquals(expectedHttpStatus, this.actualHttpStatus);
    }

    private void Then_this_fails_because(final HttpStatus expectedCause) {
        assertNull(this.unexpectedCause);
        assertNotNull(this.actualHttpStatus);
        assertEquals(expectedCause, this.actualHttpStatus);
    }

    private void Given(final ProductResource product) {
        final var restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        final var response = restTemplate.postForEntity(postProductUrl(), product, ProductResource.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    private String postProductUrl() {
        return format("%s/products", apiBaseUrl());
    }

    private void When_we_issue_an_HTTP_GET_request(final String productUrl) {
        try {
            this.unexpectedCause = null;
            this.actualHttpStatus = null;
            this.actualProductResource = null;
            final var restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
            final var response = restTemplate.getForEntity(effectiveUrl(productUrl), ProductResource.class);
            this.actualHttpStatus = response.getStatusCode();
            this.actualProductResource = response.getBody();
        } catch (final HttpServerErrorException cause) {
            this.actualHttpStatus = cause.getStatusCode();
        } catch (final HttpClientErrorException cause) {
            this.actualHttpStatus = cause.getStatusCode();
        } catch (final Exception cause) {
            this.unexpectedCause = cause;
        }
    }

    private ProductResource[] actualProductResources;

    private void When_we_issue_an_HTTP_GET_request_for_multiple_products(final String productUrl) {
        try {
            this.unexpectedCause = null;
            this.actualHttpStatus = null;
            this.actualProductResources = null;
            final var restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
            final var response = restTemplate.getForEntity(effectiveUrl(productUrl), ProductResource[].class);
            this.actualHttpStatus = response.getStatusCode();
            this.actualProductResources = response.getBody();
        } catch (final HttpServerErrorException cause) {
            this.actualHttpStatus = cause.getStatusCode();
        } catch (final Exception cause) {
            this.unexpectedCause = cause;
        }
    }

    private void Then_we_get_back_this(final ProductResource expectedProduct) {
        assertNotNull(this.actualProductResource);
        assertThat(this.actualProductResource).usingRecursiveComparison().isEqualTo(expectedProduct);
    }

    private void Then_we_get_back_these(final ProductResource... expectedProducts) {
        assertNotNull(this.actualProductResources);
        assertEquals(expectedProducts.length, this.actualProductResources.length);
        var i = 0;
        for (final var actualProductResource : this.actualProductResources) {
            assertThat(actualProductResource).usingRecursiveComparison().isEqualTo(expectedProducts[i++]);
        }
    }

    private void Given(final DevicePackageResource devicePackage) {
        final var restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        final var response = restTemplate.postForEntity(postDevicePackageUrl(devicePackage.name), devicePackage,
                DevicePackageResource.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    private String postDevicePackageUrl(final String devicePackageName) {
        return format("%s/device-packages/%s", apiBaseUrl(), devicePackageName);
    }

}

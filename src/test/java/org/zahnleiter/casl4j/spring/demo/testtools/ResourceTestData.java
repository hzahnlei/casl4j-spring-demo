package org.zahnleiter.casl4j.spring.demo.testtools;

import org.zahnleiter.casl4j.spring.demo.application.api.resource.DevicePackageResource;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.ProductResource;
import org.zahnleiter.casl4j.spring.demo.application.api.resource.VendorResource;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
public final class ResourceTestData {

    public static ProductResource new_product(final String vendorCode, final String vendorProductNumber,
            final String name, final String description, final String packageName, final int pricePerUnit) {
        return ProductResource.builder() //
                .vendorCode(vendorCode) //
                .vendorProductNumber(vendorProductNumber) //
                .name(name) //
                .description(description) //
                .packageName(packageName) //
                .pricePerUnit(pricePerUnit) //
                .build();
    }

    public static ProductResource product(final String productNumber, final String vendorCode,
            final String vendorProductNumber, final String name, final String description, final String packageName,
            final int pricePerUnit) {
        return ProductResource.builder() //
                .productNumber(productNumber) //
                .vendorCode(vendorCode) //
                .vendorProductNumber(vendorProductNumber) //
                .name(name) //
                .description(description) //
                .packageName(packageName) //
                .pricePerUnit(pricePerUnit) //
                .build();
    }

    public static ProductResource product(final String vendorCode, final String vendorProductNumber, final String name,
            final String description, final String packageName, final int pricePerUnit) {
        return new_product(vendorCode, vendorProductNumber, name, description, packageName, pricePerUnit);
    }

    public static VendorResource vendor(final String code, final String name) {
        return VendorResource.builder() //
                .code(code) //
                .name(name) //
                .build();
    }

    public static DevicePackageResource devicePackage(final String name, final String description) {
        return DevicePackageResource.builder() //
                .name(name) //
                .description(description) //
                .build();
    }

}

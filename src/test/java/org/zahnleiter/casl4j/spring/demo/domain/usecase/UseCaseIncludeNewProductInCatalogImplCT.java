package org.zahnleiter.casl4j.spring.demo.domain.usecase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.zahnleiter.casl4j.spring.demo.testtools.DomainTestData.devicePackage;
import static org.zahnleiter.casl4j.spring.demo.testtools.DomainTestData.new_product;
import static org.zahnleiter.casl4j.spring.demo.testtools.DomainTestData.vendor;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.zahnleiter.casl4j.spring.demo.domain.entity.DevicePackage;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Product;
import org.zahnleiter.casl4j.spring.demo.domain.entity.Vendor;
import org.zahnleiter.casl4j.spring.demo.domain.entity.type.VendorCode;
import org.zahnleiter.casl4j.spring.demo.domain.service.CatalogService;
import org.zahnleiter.casl4j.spring.demo.domain.service.DevicePackageService;
import org.zahnleiter.casl4j.spring.demo.domain.service.VendorListService;

/**
 * Clean Architecture Support Library for Java - Spring Demo
 * <p>
 * (c) 2021 Holger Zahnleiter, all rights reserved
 *
 * @author Holger Zahnleiter
 */
@Tag("CT")
@Tag("fast")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class UseCaseIncludeNewProductInCatalogImplCT {

    // ---- TESTCASES ---------------------------------------------------------

    @Test
    void Cannot_include_product_in_catalog_when_no_vendors_enlisted() {
        Given_no_vendors();

        When_we_include_into_our_catalog(new_product("MICR", "vendor product nr", "some product name",
                "some droduct description", "DIP16", 182));

        Then_this_fails_because("Unknown vendor MICR.");
    }

    @Test
    void Cannot_include_product_in_catalog_for_nonexisting_vendor() {
        Given(vendor("MICR", "Microchip Technology Inc."));
        Given(vendor("STMI", "STMicroelectronics"));
        Given(vendor("NXPS", "NXP Semiconductors"));
        Given(vendor("BROA", "Broadcom"));

        When_we_include_into_our_catalog(new_product(unknown_vendor_code("abcd"), "vendor product nr",
                "some product name", "some droduct description", "DIP16", 182));

        Then_this_fails_because("Unknown vendor abcd.");
    }

    @Test
    void Product_included_in_catalog_iif_vendor_exists() {
        Given(vendor("MICR", "Microchip Technology Inc."));
        Given(devicePackage("DIP28", "28 pins"));

        When_we_include_into_our_catalog(new_product("MICR", "ATMEGA8A-PU", "ATMega8A-PU",
                "8-Bit, high performance, low cost microcontroller", "DIP28", 182));

        Then_this_succeeds();
    }

    // ---- IMPLEMENTATION DETAILS --------------------------------------------

    private CatalogService catalogMock;

    private VendorListService vendorListMock;

    private DevicePackageService packageServiceMock;

    private UseCaseIncludeNewProductInCatalogImpl useCaseUnderTest;

    private static final UUID UUID_1 = UUID.fromString("10000000-1000-1000-1000-100000000000");

    private static final OffsetDateTime TIMESTAMP_1 = OffsetDateTime.of(2020, 03, 25, 15, 02, 0, 0, ZoneOffset.UTC);

    @BeforeEach
    private void setupUseCaseUnderTest() {
        this.catalogMock = Mockito.mock(CatalogService.class);
        this.vendorListMock = mock(VendorListService.class);
        this.packageServiceMock = mock(DevicePackageService.class);
        this.useCaseUnderTest = new UseCaseIncludeNewProductInCatalogImpl(this.catalogMock, this.vendorListMock,
                this.packageServiceMock, () -> UUID_1, () -> TIMESTAMP_1);
    }

    private void Given_no_vendors() {
        when(this.vendorListMock.isListed(any(VendorCode.class))) //
                .thenReturn(false);
    }

    private Exception actualCause;

    private void When_we_include_into_our_catalog(final Product product) {
        this.actualCause = null;
        try {
            this.useCaseUnderTest.include(product);
        } catch (final Exception cause) {
            this.actualCause = cause;
        }
    }

    private void Then_this_fails_because(final String expectedError) {
        assertNotNull(this.actualCause);
        assertEquals(expectedError, this.actualCause.getMessage());
    }

    private void Given(final Vendor vendor) {
        when(this.vendorListMock.isListed(vendor.code)) //
                .thenReturn(true);
    }

    private String unknown_vendor_code(final String vendorCode) {
        return vendorCode;
    }

    private void Then_this_succeeds() {
        assertNull(this.actualCause);
    }

    private void Given(final DevicePackage devicePackage) {
        when(this.packageServiceMock.exists(devicePackage.name)) //
                .thenReturn(true);
    }

}
